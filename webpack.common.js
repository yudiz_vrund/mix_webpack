const path = require('path');

module.exports = {
  entry: {
    main: './src/index.js',
    student: './src/student.js',
  },
  module: {
    rules: [
      {
        test: /\.html$/,
        use: ['html-loader'],
      },
    ],
  },
};
